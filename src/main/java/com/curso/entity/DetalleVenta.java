package com.curso.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Erick GS on 16/04/2018
 */
@Entity
public class DetalleVenta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idDetalleVenta;
    private Integer cantidad;
    @ManyToOne
    @JoinColumn(name = "id_producto", nullable = false)
    private Producto producto;
    @ManyToOne
    @JoinColumn(name = "id_venta", nullable = false)
    private Venta venta;

    public DetalleVenta() {
    }

    public int getIdDetalleVenta() {
        return idDetalleVenta;
    }

    public void setIdDetalleVenta(int idDetalleVenta) {
        this.idDetalleVenta = idDetalleVenta;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Venta getVenta() {
        return venta;
    }

    public void setVenta(Venta venta) {
        this.venta = venta;
    }

    @Override
    public String toString() {
        return "{\"DetalleVenta\":{"
                + "\"idDetalleVenta\":\"" + idDetalleVenta + "\""
                + ", \"cantidad\":\"" + cantidad + "\""
                + ", \"producto\":" + producto
                + ", \"venta\":" + venta
                + "}}";
    }
}
