package com.curso.dao;

import com.curso.entity.Producto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Erick GS on 18/04/2018.
 */
@Repository
public interface ProductoDao extends JpaRepository<Producto, Integer>{
}
