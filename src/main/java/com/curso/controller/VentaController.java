package com.curso.controller;

import com.curso.canonical.ConfirmarVentaCanonical;
import com.curso.canonical.VentaCanonical;
import com.curso.entity.Persona;
import com.curso.service.PersonaService;
import com.curso.service.VentaService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Erick GS on 18/04/2018.
 */
@RestController
@RequestMapping("/venta")
public class VentaController {

    private VentaService ventaService;

    public VentaController(VentaService ventaService) {
        this.ventaService = ventaService;
    }

    @GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<VentaCanonical>> listar(){
        List<VentaCanonical> canonicals = new ArrayList<>();
        try {
            canonicals = ventaService.listar();
        }catch(Exception e) {
            return new ResponseEntity<List<VentaCanonical>>(canonicals, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<List<VentaCanonical>>(canonicals, HttpStatus.OK);
    }

    @GetMapping(value = "/listar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VentaCanonical> listarId(@PathVariable("id") Integer id){
        VentaCanonical canonical = new VentaCanonical();
        try {
            canonical = ventaService.listarId(id);
        }catch(Exception e) {
            return new ResponseEntity<VentaCanonical>(canonical, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<VentaCanonical>(canonical, HttpStatus.OK);
    }

    @PostMapping(value = "/registrar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VentaCanonical> registrar(@RequestBody ConfirmarVentaCanonical confirmar) {
        VentaCanonical canonical = new VentaCanonical();
        try {
            canonical = ventaService.registrar(confirmar);
        } catch (Exception e) {
            return new ResponseEntity<VentaCanonical>(canonical, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<VentaCanonical>(canonical, HttpStatus.OK);
    }

}
