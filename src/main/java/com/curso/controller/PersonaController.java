package com.curso.controller;

import com.curso.entity.Persona;
import com.curso.entity.Producto;
import com.curso.service.PersonaService;
import com.curso.service.ProductoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Erick GS on 18/04/2018.
 */
@RestController
@RequestMapping("/persona")
public class PersonaController {

    private PersonaService personaService;

    public PersonaController(PersonaService personaService) {
        this.personaService = personaService;
    }

    @GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Persona>> listar(){
        List<Persona> personas = new ArrayList<>();
        try {
            personas = personaService.listar();
        }catch(Exception e) {
            return new ResponseEntity<List<Persona>>(personas, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<List<Persona>>(personas, HttpStatus.OK);
    }

    @GetMapping(value = "/listar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Persona> listarId(@PathVariable("id") Integer id){
        Persona persona = new Persona();
        try {
            persona = personaService.listarId(id);
        }catch(Exception e) {
            return new ResponseEntity<Persona>(persona, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<Persona>(persona, HttpStatus.OK);
    }

    @PostMapping(value = "/registrar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Persona> registrar(@RequestBody Persona per) {
        Persona persona = new Persona();
        try {
            persona = personaService.registrar(per);
        } catch (Exception e) {
            return new ResponseEntity<Persona>(persona, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<Persona>(persona, HttpStatus.OK);
    }

    @PutMapping(value = "/actualizar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> actualizar(@RequestBody Persona persona) {
        int resultado = 0;
        try {
            personaService.modificar(persona);
            resultado = 1;
        } catch (Exception e) {
            resultado = 0;
        }

        return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
    }

    @DeleteMapping(value = "/eliminar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> eliminar(@PathVariable Integer id) {
        int resultado = 0;
        try {
            personaService.eliminar(id);
            resultado = 1;
        } catch (Exception e) {
            resultado = 0;
        }
        return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
    }
}
