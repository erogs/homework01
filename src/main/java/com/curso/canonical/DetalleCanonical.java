package com.curso.canonical;

import java.io.Serializable;

/**
 * Created by Erick GS on 18/04/2018.
 */
public class DetalleCanonical {

    private int idProducto;
    private String nombreProducto;
    private String marcaProducto;
    private Integer cantidad;

    public DetalleCanonical(int idProducto, String nombreProducto, String marcaProducto, Integer cantidad) {
        this.idProducto = idProducto;
        this.nombreProducto = nombreProducto;
        this.marcaProducto = marcaProducto;
        this.cantidad = cantidad;
    }

    public DetalleCanonical() {
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public String getMarcaProducto() {
        return marcaProducto;
    }

    public void setMarcaProducto(String marcaProducto) {
        this.marcaProducto = marcaProducto;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public String toString() {
        return "DetalleCanonical{" +
                "idProducto=" + idProducto +
                ", nombreProducto='" + nombreProducto + '\'' +
                ", marcaProducto='" + marcaProducto + '\'' +
                ", cantidad=" + cantidad +
                '}';
    }
}
