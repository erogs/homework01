package com.curso.canonical;

import com.curso.entity.Persona;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Erick GS on 18/04/2018.
 */
public class VentaCanonical implements Serializable{

    private int ventaId;
    @JsonSerialize(using = ToStringSerializer.class)
    private LocalDateTime fecha;
    private Double importe;
    private Persona persona;
    private List<DetalleCanonical> detalleCanonical;

    public int getVentaId() {
        return ventaId;
    }

    public void setVentaId(int ventaId) {
        this.ventaId = ventaId;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public Double getImporte() {
        return importe;
    }

    public void setImporte(Double importe) {
        this.importe = importe;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public List<DetalleCanonical> getDetalleCanonical() {
        return detalleCanonical;
    }

    public void setDetalleCanonical(List<DetalleCanonical> detalleCanonical) {
        this.detalleCanonical = detalleCanonical;
    }

    @Override
    public String toString() {
        return "VentaCanonical{" +
                "ventaId=" + ventaId +
                ", fecha=" + fecha +
                ", importe=" + importe +
                ", persona=" + persona +
                ", detalleCanonical=" + detalleCanonical +
                '}';
    }
}
