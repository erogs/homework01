package com.curso.canonical;

import java.io.Serializable;

/**
 * Created by Erick GS on 18/04/2018.
 */
public class DetalleConfirmarCaninical implements Serializable{

    private int idProducto;
    private int cantidad;

    public DetalleConfirmarCaninical(int idProducto, int cantidad) {
        this.idProducto = idProducto;
        this.cantidad = cantidad;
    }

    public DetalleConfirmarCaninical() {
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public String toString() {
        return "DetalleConfirmarCaninical{" +
                "idProducto=" + idProducto +
                ", cantidad=" + cantidad +
                '}';
    }
}
