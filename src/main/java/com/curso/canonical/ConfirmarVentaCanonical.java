package com.curso.canonical;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Erick GS on 18/04/2018.
 */
public class ConfirmarVentaCanonical implements Serializable{

    private int idPersona;
    private Double importe;
    private List<DetalleConfirmarCaninical> detalle;

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public Double getImporte() {
        return importe;
    }

    public void setImporte(Double importe) {
        this.importe = importe;
    }

    public List<DetalleConfirmarCaninical> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<DetalleConfirmarCaninical> detalle) {
        this.detalle = detalle;
    }

    @Override
    public String toString() {
        return "ConfirmarVentaCanonical{" +
                "idPersona=" + idPersona +
                ", importe=" + importe +
                ", detalle=" + detalle +
                '}';
    }
}
