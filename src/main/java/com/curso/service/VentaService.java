package com.curso.service;

import com.curso.canonical.ConfirmarVentaCanonical;
import com.curso.canonical.VentaCanonical;
import com.curso.entity.Venta;

import java.util.List;

/**
 * Created by Erick GS on 18/04/2018.
 */
public interface VentaService {

    VentaCanonical registrar (ConfirmarVentaCanonical canonical);
    VentaCanonical listarId (int id);
    List<VentaCanonical> listar ();
}
