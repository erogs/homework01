package com.curso.service;

import com.curso.entity.Persona;

import java.util.List;

/**
 * Created by Erick GS on 18/04/2018.
 */
public interface PersonaService {

    Persona registrar(Persona persona);
    void modificar(Persona persona);
    void eliminar(int idPersona);
    Persona listarId(int idPersona);
    List<Persona> listar();
}
