package com.curso.service.impl;

import com.curso.dao.ProductoDao;
import com.curso.entity.Producto;
import com.curso.service.ProductoService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Erick GS on 18/04/2018.
 */
@Service
public class ProductoServiceImpl implements ProductoService{

    private ProductoDao productoDao;

    public ProductoServiceImpl(ProductoDao productoDao) {
        this.productoDao = productoDao;
    }

    @Override
    public Producto registrar(Producto producto) {
        return productoDao.save(producto);
    }

    @Override
    public void modificar(Producto producto) {
        productoDao.save(producto);
    }

    @Override
    public void eliminar(int idProducto) {
        productoDao.delete(idProducto);
    }

    @Override
    public Producto listarId(int idProducto) {
        return productoDao.findOne(idProducto);
    }

    @Override
    public List<Producto> listar() {
        return productoDao.findAll();
    }
}
