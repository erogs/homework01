package com.curso.service.impl;

import com.curso.canonical.ConfirmarVentaCanonical;
import com.curso.canonical.VentaCanonical;
import com.curso.dao.PersonaDao;
import com.curso.dao.ProductoDao;
import com.curso.dao.VentaDao;
import com.curso.entity.DetalleVenta;
import com.curso.entity.Persona;
import com.curso.entity.Venta;
import com.curso.service.VentaService;
import com.curso.util.CanonicalUtil;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Erick GS on 18/04/2018.
 */
@Service
public class VentaServiceImpl implements VentaService {

    private VentaDao ventaDao;
    private PersonaDao personaDao;
    private ProductoDao productoDao;

    public VentaServiceImpl(VentaDao ventaDao, PersonaDao personaDao, ProductoDao productoDao) {
        this.ventaDao = ventaDao;
        this.personaDao = personaDao;
        this.productoDao = productoDao;
    }

    @Override
    public VentaCanonical registrar(ConfirmarVentaCanonical canonical) {
        Venta venta = new Venta();
        venta.setFecha(LocalDateTime.now());
        venta.setImporte(canonical.getImporte());
        venta.setPersona(personaDao.findOne(canonical.getIdPersona()));
        venta.setDetalles(new ArrayList<>());
        canonical.getDetalle().forEach(item -> {
            DetalleVenta detalleVenta = new DetalleVenta();
            detalleVenta.setCantidad(item.getCantidad());
            detalleVenta.setProducto(productoDao.findOne(item.getIdProducto()));
            detalleVenta.setVenta(venta);
            venta.getDetalles().add(detalleVenta);
        });
        ventaDao.save(venta);

        return CanonicalUtil.createCanonical(venta);
    }

    @Override
    public VentaCanonical listarId(int idVenta) {
        Venta venta = ventaDao.findOne(idVenta);
        return CanonicalUtil.createCanonical(venta);
    }

    @Override
    public List<VentaCanonical> listar() {
        List<VentaCanonical> canonicals = new ArrayList<>();
        List<Venta> ventas = ventaDao.findAll();
        ventas.forEach(item -> canonicals.add(CanonicalUtil.createCanonical(item)));
        return canonicals;
    }
}
