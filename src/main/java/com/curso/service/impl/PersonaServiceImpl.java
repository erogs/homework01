package com.curso.service.impl;

import com.curso.dao.PersonaDao;
import com.curso.entity.Persona;
import com.curso.service.PersonaService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Erick GS on 18/04/2018.
 */
@Service
public class PersonaServiceImpl implements PersonaService{

    private PersonaDao personaDao;

    public PersonaServiceImpl(PersonaDao personaDao) {
        this.personaDao = personaDao;
    }

    @Override
    public Persona registrar(Persona persona) {
        return personaDao.save(persona);
    }

    @Override
    public void modificar(Persona persona) {
        personaDao.save(persona);
    }

    @Override
    public void eliminar(int idPersona) {
        personaDao.delete(idPersona);
    }

    @Override
    public Persona listarId(int idPersona) {
        return personaDao.findOne(idPersona);
    }

    @Override
    public List<Persona> listar() {
        return personaDao.findAll();
    }
}
