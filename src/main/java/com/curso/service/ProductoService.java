package com.curso.service;

import com.curso.entity.Producto;

import java.util.List;

/**
 * Created by Erick GS on 18/04/2018.
 */
public interface ProductoService {

    Producto registrar(Producto producto);
    void modificar(Producto producto);
    void eliminar(int idProducto);
    Producto listarId(int idProducto);
    List<Producto> listar();

}
