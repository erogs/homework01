package com.curso.util;

import com.curso.canonical.DetalleCanonical;
import com.curso.canonical.VentaCanonical;
import com.curso.entity.Venta;

import java.util.ArrayList;

/**
 * Created by Erick GS on 18/04/2018.
 */
public class CanonicalUtil {

    public static VentaCanonical createCanonical(Venta venta){
        if(venta == null){
            return null;
        }
        VentaCanonical canonical = new VentaCanonical();
        canonical.setVentaId(venta.getIdVenta());
        canonical.setImporte(venta.getImporte());
        canonical.setPersona(venta.getPersona());
        canonical.setFecha(venta.getFecha());
        canonical.setDetalleCanonical(new ArrayList<>());
        venta.getDetalles().forEach(item -> {
            DetalleCanonical detalleCanonical = new DetalleCanonical();
            detalleCanonical.setIdProducto(item.getProducto().getIdProducto());
            detalleCanonical.setNombreProducto(item.getProducto().getNombre());
            detalleCanonical.setMarcaProducto(item.getProducto().getMarca());
            detalleCanonical.setCantidad(item.getCantidad());
            canonical.getDetalleCanonical().add(detalleCanonical);
        });
        return canonical;
    }
}
